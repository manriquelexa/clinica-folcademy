package com.folcademy.clinica.Model.Entities;

import javax.persistence.*;

@Entity
@Table(name = "paciente")
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente")
    public Integer idpaciente;
    @Column(name = "dni")
    public String dni;
    @Column(name = "Nombre")
    public String nombre;
    @Column(name = "Apellido")
    public String apellido;
    @Column(name = "Telefono")
    public String telefono;
}
